package com.jeasy.fileattach.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.fileattach.entity.FileAttachEntity;

/**
 * 文件附件 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface FileAttachDAO extends BaseDAO<FileAttachEntity> {
}

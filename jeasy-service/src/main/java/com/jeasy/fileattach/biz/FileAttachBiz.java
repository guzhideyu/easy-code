package com.jeasy.fileattach.biz;

import com.jeasy.common.spring.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 文件附件 Biz
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class FileAttachBiz {

    public static FileAttachBiz me() {
        return SpringContextHolder.getBean(FileAttachBiz.class);
    }
}

package com.jeasy.fileattach.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.impl.BaseServiceImpl;
import com.jeasy.fileattach.dto.*;
import com.jeasy.fileattach.entity.FileAttachEntity;
import com.jeasy.fileattach.manager.FileAttachManager;
import com.jeasy.fileattach.service.FileAttachService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件附件 ServiceImpl
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Service
public class FileAttachServiceImpl extends BaseServiceImpl<FileAttachManager, FileAttachEntity, FileAttachDTO> implements FileAttachService {

    @Override
    public List<FileAttachListResDTO> list(final FileAttachListReqDTO fileattachListReqDTO) {
        return manager.list(fileattachListReqDTO);
    }

    @Override
    public List<FileAttachListResDTO> listByVersion1(final FileAttachListReqDTO fileattachListReqDTO) {
        return manager.listByVersion1(fileattachListReqDTO);
    }

    @Override
    public List<FileAttachListResDTO> listByVersion2(final FileAttachListReqDTO fileattachListReqDTO) {
        return manager.listByVersion2(fileattachListReqDTO);
    }

    @Override
    public List<FileAttachListResDTO> listByVersion3(final FileAttachListReqDTO fileattachListReqDTO) {
        return manager.listByVersion3(fileattachListReqDTO);
    }

    @Override
    public FileAttachListResDTO listOne(final FileAttachListReqDTO fileattachListReqDTO) {
        return manager.listOne(fileattachListReqDTO);
    }

    @Override
    public Page<FileAttachPageResDTO> pagination(final FileAttachPageReqDTO fileattachPageReqDTO, final Integer currentPage, final Integer pageLimit) {
        return manager.pagination(fileattachPageReqDTO, currentPage, pageLimit);
    }

    @Override
    public Boolean add(final FileAttachAddReqDTO fileattachAddReqDTO) {
        return manager.add(fileattachAddReqDTO);
    }

    @Override
    public Boolean addAllColumn(final FileAttachAddReqDTO fileattachAddReqDTO) {
        return manager.addAllColumn(fileattachAddReqDTO);
    }

    @Override
    public Boolean addBatchAllColumn(final List<FileAttachAddReqDTO> fileattachAddReqDTOList) {
        return manager.addBatchAllColumn(fileattachAddReqDTOList);
    }

    @Override
    public FileAttachShowResDTO show(final Long id) {
        return manager.show(id);
    }

    @Override
    public List<FileAttachShowResDTO> showByIds(final List<Long> ids) {
        return manager.showByIds(ids);
    }

    @Override
    public Boolean modify(final FileAttachModifyReqDTO fileattachModifyReqDTO) {
        return manager.modify(fileattachModifyReqDTO);
    }

    @Override
    public Boolean modifyAllColumn(final FileAttachModifyReqDTO fileattachModifyReqDTO) {
        return manager.modifyAllColumn(fileattachModifyReqDTO);
    }

    @Override
    public Boolean removeByParams(final FileAttachRemoveReqDTO fileattachRemoveReqDTO) {
        return manager.removeByParams(fileattachRemoveReqDTO);
    }
}

package com.jeasy.resource.biz;

import com.jeasy.common.spring.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 菜单 Biz
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class ResourceBiz {

    public static ResourceBiz me() {
        return SpringContextHolder.getBean(ResourceBiz.class);
    }
}

export default [
  {
    path: '/api',
    title: '接口文档',
    name: 'api',
    icon: 'usb',
    component: resolve => require(['@/app/code/api'], resolve)
  },
  {
    path: '/code',
    title: '代码生成',
    name: 'code',
    icon: 'code-download',
    component: resolve => require(['@/app/code'], resolve)
  }
]

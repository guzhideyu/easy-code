export default [
  {
    path: '/log',
    title: '操作日志',
    name: 'log',
    icon: 'ios-paper',
    component: resolve => require(['@/app/monitor/log'], resolve)
  },
  {
    path: '/druid',
    title: '数据监控',
    name: 'druid',
    icon: 'ios-analytics',
    component: resolve => require(['@/app/monitor/druid'], resolve)
  },
  {
    path: '/monitor',
    title: '接口监控',
    name: 'monitor',
    icon: 'usb',
    component: resolve => require(['@/app/monitor'], resolve)
  }
]

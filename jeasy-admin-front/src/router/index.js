import Vue from 'vue'
import iView from 'iview'
import VueRouter from 'vue-router'
import Util from '../libs/util'
import storage from '@/utils/helpers/storageLite'
import {adminRouter, appRouter, otherRouter} from './routes'

Vue.use(VueRouter)

// 不作为Main组件的子页面展示的页面单独写，如下
export const loginRouter = {
  path: '/login',
  name: 'login',
  meta: {
    title: 'Login - 登录'
  },
  component: resolve => {
    require(['@/views/login.vue'], resolve)
  }
}

export const page404 = {
  path: '/*',
  name: 'error_404',
  meta: {
    title: '404-页面不存在'
  },
  component: resolve => {
    require(['@/views/error_page/404.vue'], resolve)
  }
}

export const page401 = {
  path: '/401',
  meta: {
    title: '401-权限不足'
  },
  name: 'error_401',
  component: resolve => {
    require(['@/views/error_page/401.vue'], resolve)
  }
}

export const page500 = {
  path: '/500',
  meta: {
    title: '500-服务端错误'
  },
  name: 'error_500',
  component: resolve => {
    require(['@/views/error_page/500.vue'], resolve)
  }
}

export const preview = {
  path: '/preview',
  name: 'preview',
  component: resolve => {
    require(['@/views/form/article-publish/preview.vue'], resolve)
  }
}

export const locking = {
  path: '/locking',
  name: 'locking',
  component: resolve => {
    require(['@/views/main_components/locking-page.vue'], resolve)
  }
}

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
  loginRouter,
  otherRouter,
  preview,
  locking,
  ...appRouter,
  ...adminRouter,
  page500,
  page401,
  page404
]

const router = new VueRouter({
  routes: routers,
  // 当hashbang的值为true时，所有的路径都会被格式化已#!开头
  hashbang: true,
  history: true,
  saveScrollPosition: true,
  suppressTransitionError: true
})

router.beforeEach((to, from, next) => {
  iView.LoadingBar.start()
  Util.title(to.meta.title)
  if (storage.get('locking') === '1' && to.name !== 'locking') {
    // 判断当前是否是锁定状态
    next(false)
    router.replace({
      name: 'locking'
    })
  } else if (storage.get('locking') === '0' && to.name === 'locking') {
    next(false)
  } else {
    if (!storage.get('user') && to.name !== 'login') {
      // 判断是否已经登录且前往的页面不是登录页
      next({
        name: 'login'
      })
    } else if (storage.get('user') && to.name === 'login') {
      // 判断是否已经登录且前往的是登录页
      Util.title()
      next({
        name: 'home_index'
      })
    } else {
      if (Util.getRouterObjByName([otherRouter, ...appRouter, ...adminRouter], to.name).access !== undefined) {
        // 判断用户是否有权限访问当前页
        if (Util.getRouterObjByName([otherRouter, ...appRouter, ...adminRouter], to.name).access === parseInt(storage.get('access'))) {
          // 如果在地址栏输入的是一级菜单则默认打开其第一个二级菜单的页面
          Util.toDefaultPage([otherRouter, ...appRouter, ...adminRouter], to, router, next)
        } else {
          router.replace({
            name: 'error_401'
          })
          next()
        }
      } else {
        Util.toDefaultPage([otherRouter, ...appRouter, ...adminRouter], to, router, next)
      }
    }
  }
  iView.LoadingBar.finish()
})

router.afterEach(() => {
  iView.LoadingBar.finish()
  window.scrollTo(0, 0)
})

export default router

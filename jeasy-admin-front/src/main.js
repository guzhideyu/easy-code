import Vue from 'vue'
import iView from 'iview'
import router from './router'
import {adminRouter} from './router/routes'
import store from '@/store'
import App from './app.vue'
import 'iview/dist/styles/iview.css'
import '@/styles/global/index.scss'
import '@/styles/iview/index.less'

import VueI18n from 'vue-i18n'
import Locales from './locale'
import zhLocale from 'iview/src/locale/lang/zh-CN'
import enLocale from 'iview/src/locale/lang/en-US'
import zhTLocale from 'iview/src/locale/lang/zh-TW'

Vue.use(VueI18n)
Vue.use(iView)

// 自动设置语言
const navLang = navigator.language
const localLang = (navLang === 'zh-CN' || navLang === 'en-US') ? navLang : false
const lang = window.localStorage.lang || localLang || 'zh-CN'

Vue.config.lang = lang
Vue.config.productionTip = false

// 多语言配置
const locales = Locales
const mergeZH = Object.assign(zhLocale, locales['zh-CN'])
const mergeEN = Object.assign(enLocale, locales['en-US'])
const mergeTW = Object.assign(zhTLocale, locales['zh-TW'])
Vue.locale('zh-CN', mergeZH)
Vue.locale('en-US', mergeEN)
Vue.locale('zh-TW', mergeTW)

new Vue({
  el: '#app',
  router: router,
  store: store,
  render: h => h(App),
  data: {
    currentPageName: ''
  },
  mounted () {
    this.currentPageName = this.$route.name
    this.$store.commit('initCachepage')
    // 全屏相关
    document.addEventListener('fullscreenchange', () => {
      this.$store.commit('changeFullScreenState')
    })
    document.addEventListener('mozfullscreenchange', () => {
      this.$store.commit('changeFullScreenState')
    })
    document.addEventListener('webkitfullscreenchange', () => {
      this.$store.commit('changeFullScreenState')
    })
    document.addEventListener('msfullscreenchange', () => {
      this.$store.commit('changeFullScreenState')
    })
  },
  created () {
    let tagsList = []
    adminRouter.map((item) => {
      if (item.children.length <= 1) {
        tagsList.push(item.children[0])
      } else {
        tagsList.push(...item.children)
      }
    })
    this.$store.commit('setTagsList', tagsList)
  }
})

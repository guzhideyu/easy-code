import getters from './getters'
import actions from './actions'
import mutations from './mutations'

export default {
  state: {
    user: null,
    items: [],
    total: 0,
    loading: true,
    menuOptionMap: {},
    urlMenuMap: {}
  },
  getters,
  actions,
  mutations
}

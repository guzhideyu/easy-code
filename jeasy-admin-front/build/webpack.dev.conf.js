'use strict'
const utils = require('./utils')
const webpack = require('webpack')
const config = require('../config')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

// 动态向入口配置中注入 webpack-hot-middleware/client
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
  baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name])
})

module.exports = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({
      sourceMap: config.dev.cssSourceMap,
      extract: true
    })
  },
  devtool: '#cheap-module-eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),

    // 提取css为单文件
    new ExtractTextPlugin({
      filename: '[name].css',
      allChunks: true
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendors',
      filename: 'vendors.js'
    }),

    // 创建html文件，并自动将依赖写入html文件中
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/template/index.ejs',
      // true或者body：所有JavaScript资源插入到body元素的底部;
      // head: 所有JavaScript资源插入到head元素中
      // false： 所有静态资源css和JavaScript都不会注入到模板文件中
      inject: false
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new FriendlyErrorsPlugin()
  ]
})
